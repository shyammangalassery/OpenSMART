package TrafficGeneratorLFSR;
import Vector::*;
import Fifo::*;
import Randomizable::*;
import LFSR :: * ;
import Cntrs :: * ;
`include "Logger.bsv"
import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;

interface TrafficGenerator;
  method Action initialize(MeshHIdx yID, MeshWIdx xID, Bit#(32) seed);
  method ActionValue#(Flit) getFlit;
endinterface

(* synthesize *)
module mkUniformRandom(TrafficGenerator);

  LFSR#(Bit#(TAdd#(TLog#(MeshHeight),TLog#(MeshWidth)))) myIdRand <- mkLFSR_4;
  Count#(Bit#(TLog#(MeshHeight))) hresort_counter <- mkCount(0);
  Count#(Bit#(TLog#(MeshWidth))) wresort_counter <- mkCount(0);
  LFSR#(Bit#(7)) injRand <- mkFeedLFSR('h41);
//  Randomize#(Data)     injRand  <- mkConstrainedRandomizer(0, 99);


  Fifo#(1, Flit) outFlit <- mkBypassFifo;

  Reg#(Bool)     startInit            <- mkReg(False);
  Reg#(Bool)     inited	              <- mkReg(False);
  Reg#(Data)     initReg              <- mkReg(0);
  Reg#(MeshWIdx) wID                  <- mkRegU;
  Reg#(MeshHIdx) hID                  <- mkRegU;
  Reg#(Bit#(32)) rg_seed              <- mkReg(?);

  rule doInitialize(!inited && startInit);
    if(initReg == 0) begin
      myIdRand.seed(truncate(rg_seed));
      injRand.seed(truncate(rg_seed));
    end
    else if(initReg < zeroExtend(wID) + zeroExtend(hID)) begin
      injRand.next;
      myIdRand.next;
    end
    else begin
//		  $display($time,"\tINIT: injRand:%d myIdRand:%d wId:%d hId:%d",injRand.value, myIdRand.value, wID, hID);
      `logLevel(tfg,1,$format("\tINIT: injRand:%d myIdRand:%d wId:%d hId:%d",injRand.value, myIdRand.value, wID, hID))

      inited <= True;
    end
    initReg <= initReg + 1;
  endrule

  rule genFlit(inited);
    injRand.next;
    let injVar = injRand.value;
    //InjectionRate = {XX| injRate = 0.XX}
    if(injVar <= fromInteger(valueOf(InjectionRate)))
    begin
      Flit flit = ?;

      flit.vc = 0;
      myIdRand.next;
      Bit#(TAdd#(TLog#(MeshHeight),TLog#(MeshWidth))) _i = myIdRand.value;
      Bit#(TLog#(MeshWidth)) wIdRand = truncate(_i);
      Bit#(TLog#(MeshHeight)) hIdRand = truncateLSB(_i);
      if(hIdRand > fromInteger(valueOf(MeshHeight)-1)) begin
        hIdRand = hresort_counter._read;
        if (hresort_counter._read == fromInteger(valueOf(MeshHeight) -1))
          hresort_counter._write(0);
        else
          hresort_counter.incr(1);
      end
      if(wIdRand > fromInteger(valueOf(MeshWidth)-1)) begin
        wIdRand = wresort_counter._read;
        if (wresort_counter._read == fromInteger(valueOf(MeshWidth) -1))
          wresort_counter._write(0);
        else
          wresort_counter.incr(1);
      end
//      if(wIdRand +1 == MeshHeight)
//        wIdRand = wIdRand - 1;
//      if(hIdRand != 0)
//        hIdRand = hIdRand - 1;

      MeshWIdx wDest =pack(zeroExtend(wIdRand ));
      DirIdx xDir = (wDest>wID)? dIdxEast:dIdxWest;
      MeshWIdx xHops = (wDest > wID)? (wDest-wID) : (wID-wDest);

      MeshHIdx hDest =pack(zeroExtend(hIdRand));
      DirIdx yDir = (hDest>hID)? dIdxSouth:dIdxNorth;
      MeshHIdx yHops = (hDest > hID)? (hDest-hID) : (hID-hDest);

      //Look-ahead routing
      //It is modified for SMART
//        flit.routeInfo.numXhops = (wDest > wID)? (wDest-wID)-1 : (wID-wDest)-1;
      flit.routeInfo.dirX = (wDest > wID)? WE_ : EW_;
      flit.routeInfo.numXhops = xHops;

      //Y-axis direction
//      let hDest <- hIdRand.next;

      //It is modified for SMART
//      flit.routeInfo.numYhops = (hDest>hID)? (hDest-hID)-1: (hID-hDest)-1;
      flit.routeInfo.dirY = (hDest > hID)? NS_ : SN_;
      flit.routeInfo.numYhops = yHops;

      //Decides initial direction
      if(wDest != wID) begin// Initial direction: X
        flit.routeInfo.nextDir = (wDest > wID)? east_ : west_;
      end
      else if(hDest !=hID) begin //Initial direction: Y
        flit.routeInfo.nextDir = (hDest > hID)? south_:north_;
      end
      else begin
        flit.routeInfo.nextDir = local_;
	  end

      flit.flitType = HeadTail;
      `ifdef DETAILED_STATISTICS
      flit.stat.hopCount = 0;
      flit.stat.dstX = wDest;
      flit.stat.dstY = hDest;
      flit.stat.srcX = wID;
      flit.stat.srcY = hID;
      `endif

      if(flit.routeInfo.nextDir != local_) begin
        outFlit.enq(flit);
      end
    end// Injection rate if ends
  endrule


  method Action initialize(MeshHIdx yID, MeshWIdx xID, Bit#(32) seed) if(!inited && !startInit);
    startInit <= True;
    hID <= yID;
    wID <= xID;
    rg_seed <= seed;
  endmethod

  method ActionValue#(Flit)	getFlit if(inited);
    outFlit.deq;
    return outFlit.first;
  endmethod
endmodule



module mkBitComplement(TrafficGenerator);

  Randomize#(Data)     injRand  <- mkConstrainedRandomizer(0, 99);

  Fifo#(1, Flit) outFlit <- mkBypassFifo;

  Reg#(Bool)     startInit            <- mkReg(False);
  Reg#(Bool)     inited	              <- mkReg(False);
  Reg#(Data)     initReg              <- mkReg(0);
  Reg#(MeshWIdx) wID                  <- mkRegU;
  Reg#(MeshHIdx) hID                  <- mkRegU;
  Reg#(Bit#(32)) rg_seed              <- mkReg(?);

  rule doInitialize(!inited && startInit);
    if(initReg == 0) begin
      injRand.cntrl.init;
    end
    else if(initReg < zeroExtend(wID) + zeroExtend(hID)) begin
      let injRnd <- injRand.next;
    end
    else begin
      inited <= True;
    end
    initReg <= initReg + 1;
  endrule

  rule genFlit(inited);
    let injVar <- injRand.next;
    //InjectionRate = {XX| injRate = 0.XX}
    if(injVar < fromInteger(valueOf(InjectionRate)))
    begin
      Flit flit = ?;

      flit.vc = 0;

      let wDest = fromInteger(valueOf(MeshWidth)) - 1 - wID;
      let hDest = fromInteger(valueOf(MeshHeight)) - 1 - hID;

      DirIdx xDir = (wDest>wID)? dIdxEast:dIdxWest;
      MeshWIdx xHops = (wDest > wID)? (wDest-wID) : (wID-wDest);

      DirIdx yDir = (hDest>hID)? dIdxSouth:dIdxNorth;
      MeshHIdx yHops = (hDest > hID)? (hDest-hID) : (hID-hDest);

      //Look-ahead routing
//        flit.routeInfo.numXhops = (wDest > wID)? (wDest-wID)-1 : (wID-wDest)-1;
      flit.routeInfo.dirX = (wDest > wID)? WE_ : EW_;
      flit.routeInfo.numXhops = xHops;

      //Y-axis direction
//      let hDest <- hIdRand.next;

//      flit.routeInfo.numYhops = (hDest>hID)? (hDest-hID)-1: (hID-hDest)-1;
      flit.routeInfo.dirY = (hDest > hID)? NS_ : SN_;
      flit.routeInfo.numYhops = yHops;

      //Decides initial direction
      if(wDest != wID) begin// Initial direction: X
        flit.routeInfo.nextDir = (wDest > wID)? east_ : west_;
      end
      else if(hDest !=hID) begin //Initial direction: Y
        flit.routeInfo.nextDir = (hDest > hID)? south_:north_;
      end
      else begin
        flit.routeInfo.nextDir = local_;
	  end

      flit.flitType = HeadTail;
      `ifdef DETAILED_STATISTICS
      flit.stat.hopCount = 0;
      flit.stat.dstX = wDest;
      flit.stat.dstY = hDest;
      flit.stat.srcX = wID;
      flit.stat.srcY = hID;
      `endif

//      $display("TrafficGenerator: from(%d, %d) send to (%d, %d).\n  Initial direction: %d\n  NumXhops = %d, NumYhops = %d",hID, wID, hDest, wDest, dir2Idx(flit.routeInfo.nextDir), xHops, yHops);

      if(flit.routeInfo.nextDir != local_) begin
        outFlit.enq(flit);
      end
    end// Injection rate if ends
  endrule


  method Action initialize(MeshHIdx yID, MeshWIdx xID, Bit#(32) seed) if(!inited && !startInit);
    startInit <= True;
    hID <= yID;
    wID <= xID;
    rg_seed <= seed;
  endmethod

  method ActionValue#(Flit)	getFlit if(inited);
    outFlit.deq;
    return outFlit.first;
  endmethod
endmodule

endpackage
