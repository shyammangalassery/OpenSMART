import Vector::*;

//User parameters
//typedef 100000  BenchmarkCycle;
typedef 100000  BenchmarkCycle;

typedef 128      DataSz;
typedef 1       NumFlitsPerDataMessage;
typedef 1       NumFlitsPerControlMessage;

//Number of VNETs
typedef 3       NumVNETs;
typedef 2       VCperVNET;
typedef 6       UserHPCMax;

typedef 2       MeshWidth;
typedef 2       MeshHeight;
typedef TMul#(NumVNETs, VCperVNET)       NumUserVCs;
// This value should be required injection ratio*127.
// If you want 20% injection rate the following variable should be set to ceil(0.2x127) = 26.
typedef 3     InjectionRate;

// to set the depth of queueing buffer (garnet has infinite queueing depth)
// if there is no space to queue(tempFifo in TrafficGeneratorUnit),
// a packet won't get injected regardless of injection rate.
typedef 4      NumTrafficGeneratorBufferSlots;

RoutingAlgorithms currentRoutingAlgorithm = XY_;

///////////////////////////////////////////////////////////////
//Fixed and derived Types

typedef enum {XY_, YX_} RoutingAlgorithms deriving(Bits, Eq);
typedef TMul#(MeshWidth, MeshHeight) NumMeshNodes;

typedef	Bit#(DataSz) Data;

//Dimensions, fixed for mesh network
typedef 5                  NumPorts;       //N, E, S, W, L
typedef TSub#(NumPorts, 1) NumNormalPorts; //N, E, S, W

typedef NumPorts           MaxNumPorts;    //For arbitrary topology
typedef NumNormalPorts     MaxNumNormalPorts;

//Mesh dimensions
typedef	TAdd#(1, TLog#(MeshWidth))	MeshWidthBitSz;
typedef	TAdd#(1, TLog#(MeshHeight))	MeshHeightBitSz;

typedef	Bit#(MeshWidthBitSz)	MeshWIdx;
typedef	Bit#(MeshHeightBitSz)	MeshHIdx;

typedef struct {
	MeshWIdx x_len;
	MeshHIdx y_len;
} Len deriving(Bits, Eq);

interface NtkArbiter#(numeric type numRequesters);
  method Action                            initialize;
  method ActionValue#(Bit#(numRequesters)) getArbit(Bit#(numRequesters) reqBit);
endinterface
