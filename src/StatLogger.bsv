import Vector::*;
import Fifo::*;
import DReg::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

interface StatLogger;
  method Action incSendCount(MsgType msg);
  method Action incRecvCount(MsgType msg);
  method Action incLatencyCount(Data latency);
  method Action incHopCount(Data hopCount);
  method Action incInflightLatencyCount(Data latency);

  method Vector#(NumVNETs, Data) getSendCount;
  method Vector#(NumVNETs, Data) getRecvCount;
  method Data getLatencyCount;
  method Data getHopCount;
  method Data getInflightLatencyCount;
endinterface

(* synthesize *)
module mkStatLogger(StatLogger);

  Vector#(NumVNETs, Reg#(Data)) send_count             <- replicateM(mkReg(0));
  Vector#(NumVNETs, Reg#(Data)) recv_count             <- replicateM(mkReg(0));
  Reg#(Data) latency_count          <- mkReg(0);
  Reg#(Data) hop_count              <- mkReg(0);
  Reg#(Data) inflight_latency_count <- mkReg(0);


  method Action incSendCount(MsgType msg);
		let lv_vnet = (msg);
    send_count[lv_vnet] <= send_count[lv_vnet] + 1;
  endmethod

  method Action incRecvCount(MsgType msg);
		let lv_vnet = (msg);
    recv_count[lv_vnet] <= recv_count[lv_vnet] + 1;
  endmethod

  method Action incLatencyCount(Data latency);
    latency_count <= latency_count + latency;
  endmethod

  method Action incHopCount(Data hopCount);
    hop_count <= hop_count + hopCount;
  endmethod

  method Action incInflightLatencyCount(Data latency);
    inflight_latency_count <= inflight_latency_count + latency;
  endmethod

  //function Data f1(Integer i) = send_count[i];
  //interface getSendCount = genWith(f1);

//	function Data f2(Integer i) = recv_count[i];
//	interface getRecvCount = genWith(f2);
  method Vector#(NumVNETs, Data) getSendCount;
			Vector#(NumVNETs, Data) lv_sendcount;
			for(Integer k=0; k<valueOf(NumVNETs); k=k+1) begin
					lv_sendcount[k] = send_count[k];
			end
			return lv_sendcount;
	endmethod

  method Vector#(NumVNETs, Data) getRecvCount;
			Vector#(NumVNETs, Data) lv_recvcount;
			for(Integer k=0; k<valueOf(NumVNETs); k=k+1) begin
					lv_recvcount[k] = recv_count[k];
			end
			return lv_recvcount;
	endmethod
  method Data getLatencyCount = latency_count;
  method Data getHopCount = hop_count;
  method Data getInflightLatencyCount = inflight_latency_count;

	//interface getSendCount = genWith(f1);
	//interface getRecvCount = genWith(f2);

endmodule
