#!/usr/bin/python3
# This python file generates a "average latency vs injection rate" plot.

import numpy as np
import matplotlib.pyplot as plt
#import matplotlib.pyplot.plot
plt.style.use('seaborn-whitegrid')

inj_rate = []
fname_lat = 'log_total_lat.txt'
fname_packet = 'log_total_inj_packets.txt'
with open(fname_lat) as f:
  content_lat = f.readlines()
with open(fname_packet) as f:
  content_packet = f.readlines()

for i in range(1,len(content_lat)):
  inj_rate.append(0.03*i)


# convert into a list
content_lat = [x.strip() for x in content_lat]
content_packet = [x.strip() for x in content_packet]
# remove the first element as it is empty line
content_lat.remove(content_lat[0])
content_packet.remove(content_packet[0])
content = []
for i,x in enumerate(content_lat):
  content_lat[i] = float(content_lat[i])
  content_packet[i] = float(content_packet[i])
  content.append(content_lat[i]/content_packet[i])
  print("inj_rate : " + str(0.03 * (i+1)) + " has latency: " + str(content[i]))

# now content on y axis (avg latency values), inj_rate on x_axis
plt.plot(inj_rate,content)
plt.xlabel("injection rate")
plt.ylabel("average latency")
plt.title("latency vs injection rate curve")
plt.show()
