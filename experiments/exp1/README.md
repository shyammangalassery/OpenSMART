## Sweep Analysis

### Aim :
To determine the `saturation throughput` and `zero load latency` of any Network on Chip model with variation in TGBS(traffic generator buffer slots)

### Network Configuration : 
3*3 mesh, XY routing, simulation time = 100,000 cycles, traffic pattern = Uniform Random using Constrained Randomizer, 3VCs / input port 

### Some Essentials: 
* Performance of network is measured in terms of network latency or accepted traffic[1].

* Zero-load latency is the latency experienced by a packet when there are no other packets in the network. It provides a lower bound on average message latency and is found by taking the average distance (given in terms of network hops) a message will travel times the latency to traverse a single hop.

* In addition to providing ultra-low latency communication, networks must also deliver high throughput.

* A high saturation throughput indicates that the network can accept a large amount of traffic before all packets experience very high latencies, sustaining higher bandwidth.
*  Network latency in multiples of 100 or greater than 3 times average latency, generally means that the network has entered into saturation. 

* BSV provides a software simulation framework which is used in OpenSMART to study the latency and throughput characteristics of the NoC. [2]
* The testbench injects 1-flit packets from each IP at increasing injection rates with user-specified traffic patterns.
* Throughput is estimated by collecting the number of injected and received flits at each host IP port.
* While garnet models infinite queues at the source to maintain injection at the user-specified rate without stalling, in OpenSMART, however, the injector stalls once the network cannot accept more packets, like a real IP.
* TGBS determine the depth of queue where flits get enqueued/ingressed before entering the local input port. Total delay is calculated by estimating the average end-to-end cycles from the ingress NIC, through the NoC, to the egress NI.
* At high injection rates, when network is working at full capacity, the TGBS gets full and no forthcoming packet get's injected.


### Steps followed : 
* Run OpenSMART software framework 30 times for injection rates in this domain [0.03, 0.9] for step size of 0.03.

* The bash script `scripts/extract_lat.sh` does this and stores {total packet latency, total injected packets, total received packets} in files 'log_total_lat.txt', 'log_total_inj_packets.txt', 'log_total_recv_packets.txt'.

* Following network configurations are chosen for analysis:
1.  simulation cycles = 100k , network model = 2-cycle default router, TGBS = 4
2.  simulation cycles = 100k, netowrk model = 2-cycle default router, TGBS = 100
3.  8*8 mesh, simulation cycles = 100k , network model = 2-cycle default router, TGBS = 4
  
* The python script `scripts/sweep_inj_rate_plot.py` plots the curve for above cases in one image.

### Observation and Results :
* `Figure1` : presents the graph for (1). It can be inferred that : 
    *  At low load injection rate of 0.03, the average latency is 8.90 cycles(starting point of curve).
    *  Saturation throughput is not reached till 0.9 injection rate (packet/node/cycle).
    
* Figure1 : ![Figure1](./pictures/tgbs4.jpg "Figure1 : TGBS4")

* `Figure2` : presents the graph for (2). It can be inferred that : 
    *  Saturation throughput is reached at 0.7 injection rate (packet/node/cycle).
    
* Figure2 : ![Figure2](./pictures/tgbs100.jpg "Figure1 : TGBS100")

* `Figure3` : presents the graph for (3). It can be inferred that : 
    *  Average latency begins to saturate at injection rate 0.5 packet/node/cycle.
    *  This is plausible due to the fact that network operates at full capacity and every packet getting injected takes the same average delay, regardless of injection rate.
    
* Figure3 : ![Figure3](./pictures/8x8_tgbs4.jpg "Figure3 : 8*8 TGBS100")






### References :
* [1] https://doi.org/10.2200/S00772ED1V01Y201704CAC040
* [2] https://ieeexplore.ieee.org/document/7975291
