
package TL_testbench;
import Vector::*;
import GetPut ::*;
import Fifo::*;
import Connectable::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

import Network::*;
import TrafficGeneratorUnit::*;
import TrafficGeneratorBuffer::*;
import CreditUnit::*;
import StatLogger::*;

import NIC_interface ::*;
import Tilelink_Types ::*;

`include "system.defines"

interface Ifc_TL_TestBench;
  `ifdef  monitor_link_utilisation
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    (* always_ready *)
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress;
  `endif
endinterface

(* synthesize *)
module mkTL_testBench(Ifc_TL_TestBench);

  /********************************* States *************************************/
  Reg#(Data) clkCount  <- mkReg(0);
  Reg#(Bool) inited    <- mkReg(False);
  Reg#(Data) initCount <- mkReg(0);


  Vector#(MeshWidth, Ifc_Master_link#(`PADDR, `LANE_WIDTH, `BURST_SIZE))    m_xactor       
																								<- replicateM(mkMasterXactor(True, True));
  Vector#(MeshWidth, Ifc_Slave_link#(`PADDR, `LANE_WIDTH, `BURST_SIZE))    s_xactor       
																								<- replicateM(mkSlaveXactor(True, True));

  Vector#(MeshHeight, Vector#(MeshWidth, ReverseCreditUnit))  creditUnits    <- replicateM(replicateM(mkReverseCreditUnit));
  Vector#(MeshHeight, Vector#(MeshWidth, StatLogger))         statLoggers    <- replicateM(replicateM(mkStatLogger));

  /******************************** Submodule ************************************/

  Network meshNtk <- mkNetwork;

  rule init(!inited);
    if(initCount == 0) begin
      clkCount <= 0;
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        // initializing the source node for traffic
				m_xactor[j].core_side.coords(Len {x_len:0, y_len:fromInteger(j)});
				s_xactor[j].core_side.coords(Len {x_len:1, y_len:fromInteger(j)});
       //`ifdef LFSR
       // trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j), fromInteger(k));
       // `else
       // trafficGeneratorUnits[i][j].initialize(fromInteger(i), fromInteger(j));
       // `endif
      end
    end

    initCount <= initCount + 1;
    if(meshNtk.isInited && initCount > fromInteger(valueOf(MeshHeight)) + fromInteger(valueOf(MeshWidth)))  begin
      inited <= True;
    end
  endrule

  rule doClkCount(inited && clkCount < fromInteger(valueOf(BenchmarkCycle)));
    if(clkCount % 10000 == 0) begin
      $display("Elapsed Simulation Cycles: %d",clkCount);
    end
    clkCount <= clkCount + 1;
  endrule


	rule rl_send_req_through_tilelink;
		A_channel#(`PADDR, `LANE_WIDTH, `BURST_SIZE) packet = A_channel {a_opcode : Acquire,
																	a_param : 0,
																	a_size : 3,
																	a_source : 2,
																	a_address : 'h8200000,
																	a_mask : '1,
																	a_data : 0}; 
		m_xactor[1].core_side.master_req_A.put(packet);
		$display("The address sent %h", packet.a_address);
																	
	endrule

	rule rl_resp_from_tilelink;
		let packet <- s_xactor[1].core_side.slave_req_A.get();
		$display("The address received %h", packet.a_address);
	endrule

  //rule finishBench(inited && clkCount == fromInteger(valueOf(BenchmarkCycle)));
  //    Data res = 0;
  //    Data send = 0;
  //    Data hop = 0;
  //    Data remainingFlits = 0;
  //    Data inflight = 0;
  //    Bit#(64) resLatency = 0;

  //    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
  //      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
  //        Data sendCount = statLoggers[i][j].getSendCount;
  //        Data recvCount = statLoggers[i][j].getRecvCount;
  //        Data latencyCount = statLoggers[i][j].getLatencyCount;
  //        Data remFlits = trafficGeneratorBufferUnits[i][j].getRemainingFlitsNumber;
////          Data extraLatency = trafficGeneratorBufferUnits[i][j].getExtraLatency(clkCount);
  //        Data hopCount = statLoggers[i][j].getHopCount;
  //        Data inflightCycle = statLoggers[i][j].getInflightLatencyCount;

  //        $display("send_count[%2d][%2d] = %10d, recv_count[%2d][%2d] = %10d", i, j, sendCount, i, j, recvCount);

  //        send = send + sendCount;
  //        res = res + recvCount;
  //        resLatency = resLatency + zeroExtend(latencyCount);// + zeroExtend(extraLatency);
  //        remainingFlits = remainingFlits + remFlits;
  //        hop = hop + hopCount;
  //        inflight = inflight + inflightCycle;

	//    end
  //    end

  //    $display("Elapsed clock cycles: %d", clkCount);
  //    $display("Total injected packet: %d",send);
  //    $display("Total received packet: %d",res);
  //    $display("Total latency: %d", resLatency);
  //    $display("Total hopCount: %d", hop);
  //    $display("Total inflight latency: %d", inflight);
  //    $display("Number of remaiing Flits in traffic generator side: %d", remainingFlits);
  //    $finish;
  //endrule


  //Credit Links
  //for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
  //  for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin

  //    mkConnection(creditUnits[i][j].getCredit,
  //                   meshNtk.ntkPorts[i][j].putCredit);

  //    mkConnection(meshNtk.ntkPorts[i][j].getCredit,
  //                   trafficGeneratorUnits[i][j].putVC);
  //  end
  //end


	for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
		mkConnection(m_xactor[i].fabric_side, meshNtk.ntkPorts[0][i]);
		mkConnection(meshNtk.ntkPorts[1][i], s_xactor[i].fabric_side);
	end

  `ifdef  monitor_link_utilisation
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumPorts,Bool))) monitor_link_utilisation = meshNtk.monitor_link_utilisation;
  `endif
  // `ifdef monitor_hop_counts // encode hop count as destination id of outgoing flit. id = node_id implies no flit.
  //   (* always_ready *)
  //   method Vector#(MeshHeight,Vector#(MeshWidth,Bit#(TLog#(Mul#(MeshHeight,MeshWidth))))) monitor_traffic_latency;
  // `endif
  `ifdef monitor_flit_ingress
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_ingress = meshNtk.monitor_node_ingress;
  `endif
  `ifdef monitor_flit_egress
    method Vector#(MeshHeight,Vector#(MeshWidth,Vector#(NumVCs,Bool))) monitor_node_egress = meshNtk.monitor_node_egress;
  `endif

endmodule
endpackage : TL_testbench