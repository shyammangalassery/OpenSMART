/*
Copyright (c) 2013, IIT Madras
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

*  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*  Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tilelink_Types;

`include "system.defines"
import GetPut ::*;
import FIFO ::*;
import SpecialFIFOs ::*;
import Connectable ::*;

import NIC_interface ::*;
import MessageTypes ::*;
import Types ::*;
import RoutingTypes ::*;
import address_map ::*;

`define TILEUH

Integer v_lane_width = valueOf(`LANE_WIDTH);

typedef enum {	Get_data=0
				,GetWrap=1
				,PutPartialData=2
				,PutFullData=3
				,ArithmeticData=4
				,LogicalData=5
				,Intent=6				
				,Acquire=7
} Opcode deriving(Bits, Eq, FShow);			
			
typedef enum {	AccessAck
				,AccessAckData
				,HintAck
				,Grant
				,GrantData
} D_Opcode deriving(Bits, Eq, FShow);			

typedef enum { Min,
			   Max,
			   MinU,
			   MaxU,
			   ADD 
} Param_arith deriving(Bits, Eq, FShow);

typedef enum { Min,
			   Max,
			   MinU,
			   MaxU,
			   ADD 
} Param_logical deriving(Bits, Eq, FShow);

typedef Bit#(3) Param;
typedef Bit#(4) Data_size; //In bytes
typedef Bit#(2) M_source;
typedef Bit#(5) S_sink;
typedef Bit#(`PADDR) Address_width;
typedef Bit#(`LANE_WIDTH) Mask;
typedef Bit#(TMul#(8,`LANE_WIDTH)) Data;

/* The A-channel is responsible for the master requests. The channel is A is split in control section(c) 
data section(d) where the read masters only use control section and write masters use both. For the slave side
where it receives the request has the channel A intact.
*/
typedef struct { 
		Opcode 			     a_opcode;                 //The opcode specifies if write or read requests
		Param  			     a_param;                  //Has the encodings for atomic transfers
		Bit#(z) 			 a_size;                   //The transfer size in 2^a_size bytes. The burst is calculated from here. if this is >3 then its a burst
		M_source 		     a_source;                 //Master ID
		Bit#(a)	 			 a_address;                //Address for the request
} A_channel_control#(numeric type a, numeric type z)  deriving(Bits, Eq, FShow);
		
typedef struct { 
		Bit#(TDiv#(w,8)) 			 a_mask;           //8x(bytes in data lane) 1 bit mask for each byte 
		Bit#(w)						 a_data;			//data for the request	
} A_channel_data#(numeric type w) deriving(Bits, Eq, FShow);

typedef struct { 
		Opcode 			     a_opcode;
		Param  			     a_param;
		Bit#(z)				 a_size;
		M_source 		     a_source;
		Bit#(a)				 a_address;
		Bit#(TDiv#(w,8))	 a_mask;
		Bit#(w)				 a_data;	
} A_channel#(numeric type a, numeric type w, numeric type z) deriving(Bits, Eq, FShow);

//cache-coherence channels
typedef struct {                                        
		Opcode 			     b_opcode;
		Param  			     b_param;
		Bit#(z)				 b_size;
		M_source 		     b_source;
		Bit#(a)				 b_address;
		Bit#(TDiv#(w,8))	 b_mask;
		Bit#(w)				 b_data;	
} B_channel#(numeric type a, numeric type w, numeric type z) deriving(Bits, Eq, FShow);

//cache-coherence channels
typedef struct { 
		Opcode 			     c_opcode;
		Param  			     c_param;
		Bit#(z)				 c_size;
		M_source 		     c_source;
		Bit#(a)				 c_address;
		Bit#(w)				 c_data;	
		Bool				 c_client_error;
} C_channel#(numeric type a, numeric type w, numeric type z) deriving(Bits, Eq);

//The channel D is responsible for the slave responses. It has the master ids and slave ids carried through the channel
typedef struct { 
		D_Opcode		d_opcode;                     //Opcode encodings for responses with data or just ack
		Param  			d_param;
		Bit#(z)			d_size;
		M_source 		d_source;
		S_sink			d_sink;
		Bit#(w)			d_data;	
		Bool			d_error;
} D_channel#(numeric type w, numeric type z) deriving(Bits, Eq, FShow);

typedef struct { 
		S_sink					 d_sink;
} E_channel deriving(Bits, Eq);

interface Ifc_core_side_master_link#(numeric type a, numeric type w, numeric type z);

	//Towards the master
	interface Put#(A_channel#(a,w,z)) master_req_A;
	interface Get#(B_channel#(a,w,z)) master_resp_B;
	interface Put#(C_channel#(a,w,z)) master_req_C;
	interface Get#(D_channel#(w,z)) master_resp_D;
	interface Put#(E_channel) master_ack_E;
	method Action coords(Len ids);

endinterface

interface Ifc_fabric_side_master_link#(numeric type a, numeric type w, numeric type z);

	//Towards the fabric
	interface Get#(A_channel#(a,w,z)) fabric_req_A;
	interface Put#(B_channel#(a,w,z)) fabric_resp_B;
	interface Get#(C_channel#(a,w,z)) fabric_req_C;
	interface Put#(D_channel#(w,z)) fabric_resp_D;
	interface Get#(E_channel) fabric_ack_E;
	method Len coords;

endinterface

//--------------------------------------Master Xactor--------------------------------------//
/* This is a xactor interface which connects core and master side of the fabric*/
interface Ifc_Master_link#(numeric type a, numeric type w, numeric type z);

interface Ifc_core_side_master_link#(a,w,z) core_side;
interface Ifc_fabric_side_master_link#(a,w,z) fabric_side;

endinterface

/* Master transactor - should be instantiated in the core side and the fabric side interface of
of the xactor should be exposed out of the core*/
module mkMasterXactor#(Bool xactor_guarded, Bool fabric_guarded)(Ifc_Master_link#(a,w,z));

//Created a pipelined version that will have a critical path all along the bus. If we want to break the path we can 
//we should trade if off with area using the 2-sized FIFO 
`ifdef TILELINK_LIGHT
	FIFOF#(A_channel#(w)) ff_xactor_A <- mkGFIFOF(xactor_guarded, fabric_guarded);   //data split of A-channel
	FIFOF#(B_channel#(a,w,z)) ff_xactor_B <- mkGFIFOF(xactor_guarded, fabric_guarded); //B-channel
	FIFOF#(C_channel#(a,w,z)) ff_xactor_C <- mkGFIFOF(xactor_guarded, fabric_guarded); //C-channel
	FIFOF#(D_channel#(w,z)) ff_xactor_D <- mkGFIFOF(xactor_guarded, fabric_guarded); //resp channel D-channel exposed out
	FIFOF#(E_channel) ff_xactor_E <- mkGFIFOF(xactor_guarded, fabric_guarded); //ack channel E-channel exposed out
`else
	FIFO#(A_channel#(a,w,z)) ff_xactor_A <- mkSizedFIFO(2);
	FIFO#(B_channel#(a,w,z)) ff_xactor_B <- mkSizedFIFO(2);
	FIFO#(C_channel#(a,w,z)) ff_xactor_C <- mkSizedFIFO(2);
	FIFO#(D_channel#(w,z)) ff_xactor_D <- mkSizedFIFO(2);
	FIFO#(E_channel) ff_xactor_E <- mkSizedFIFO(2);
`endif

	Reg#(Bit#(z)) rg_burst_counter <- mkReg(0);
	Reg#(Bool) rg_burst[2] <- mkCReg(2,False);

	Reg#(MeshWIdx) rg_x_id <- mkRegU();
	Reg#(MeshHIdx) rg_y_id <- mkRegU();

// If it is a burst dont ask for address again. This rule about calculating the burst and control split of A-channel remains 
//constant till the burst finishes.
	rule rl_xactor_to_fabric_data;
		let req_addr = ff_xactor_A.first;
		Bit#(z) burst_size = 1;										  //The total number of bursts
		Bit#(z) transfer_size = req_addr.a_size;                        //This is the total transfer size including the bursts
		if(!rg_burst[0]) begin
			if(transfer_size > 3) begin
				rg_burst[0] <= True;
				transfer_size = transfer_size - 3;
				burst_size = burst_size << transfer_size;
				rg_burst_counter <= burst_size - 1;
			end
		end
		else begin
			rg_burst_counter <= rg_burst_counter - 1;                         
			if(rg_burst_counter==1)
				rg_burst[0] <= False;
		end

	endrule
	
	interface core_side = interface Ifc_core_side_master_link
		interface master_req_A = toPut(ff_xactor_A);
		interface master_resp_B = toGet(ff_xactor_B);
		interface master_req_C = toPut(ff_xactor_C);
		interface master_resp_D = toGet(ff_xactor_D);
		interface master_ack_E = toPut(ff_xactor_E);
		method Action coords(Len ids);
			rg_x_id <=ids.x_len; 
			rg_y_id <=ids.y_len; 
		endmethod

	endinterface;

	interface fabric_side = interface Ifc_fabric_side_master_link 
		interface fabric_req_A = toGet(ff_xactor_A);
		interface fabric_resp_B = toPut(ff_xactor_B);
		interface fabric_req_C  = toGet(ff_xactor_C);
		interface fabric_resp_D = toPut(ff_xactor_D);
		interface fabric_ack_E = toGet(ff_xactor_E);
		method Len coords;
			return Len {x_len : rg_x_id, y_len : rg_y_id};
		endmethod
	endinterface;
	
endmodule

//------------------------------------------------------------------------------------------------------------------//


//------------------------------------------------------Slave Xactor------------------------------------------------//

//To be connected to slave side 
interface Ifc_core_side_slave_link#(numeric type a, numeric type w, numeric type z);
	interface Get#(A_channel#(a,w,z)) slave_req_A;
	interface Put#(B_channel#(a,w,z)) slave_resp_B;
	interface Get#(C_channel#(a,w,z)) slave_req_C;
	interface Put#(D_channel#(w,z)) slave_resp_D;
	interface Get#(E_channel) slave_ack_E;
	method Action coords(Len ids);
endinterface

//To be connected to fabric side
interface Ifc_fabric_side_slave_link#(numeric type a, numeric type w, numeric type z);
	//Doesn't need to have control and data signals separated as slaves get A_channel packet intact
	interface Put#(A_channel#(a,w,z)) fabric_req_A;
	interface Get#(B_channel#(a,w,z)) fabric_resp_B;
	interface Put#(C_channel#(a,w,z)) fabric_req_C;
	interface Get#(D_channel#(w,z)) fabric_resp_D;
	interface Put#(E_channel) fabric_ack_E;
	method Len coords;
endinterface

interface Ifc_Slave_link#(numeric type a, numeric type w, numeric type z);
	interface Ifc_core_side_slave_link#(a,w,z) core_side;
	interface Ifc_fabric_side_slave_link#(a,w,z) fabric_side;
endinterface

module mkSlaveXactor#(Bool xactor_guarded, Bool fabric_guarded)(Ifc_Slave_link#(a,w,z));

//Can choose between 2-sized FIFO and pipeline FIFO just like the master xactor
`ifdef TILELINK_LIGHT
	FIFOF#(A_channel#(a,w,z)) ff_xactor_A <- mkGFIFOF(xactor_guarded, fabric_guarded);
	FIFOF#(B_channel#(a,w,z)) ff_xactor_B <- mkGFIFOF(xactor_guarded, fabric_guarded);
	FIFOF#(C_channel#(a,w,z)) ff_xactor_C <- mkGFIFOF(xactor_guarded, fabric_guarded);
	FIFOF#(D_channel#(w,z)) ff_xactor_D <- mkGFIFOF(xactor_guarded, fabric_guarded);
	FIFOF#(E_channel) ff_xactor_E <- mkGFIFOF(xactor_guarded, fabric_guarded);
`else
	FIFO#(A_channel#(a,w,z)) ff_xactor_A <- mkSizedFIFO(2);
	FIFO#(B_channel#(a,w,z)) ff_xactor_B <- mkSizedFIFO(2);
	FIFO#(C_channel#(a,w,z)) ff_xactor_C <- mkSizedFIFO(2);
	FIFO#(D_channel#(w,z)) ff_xactor_D <- mkSizedFIFO(2);
	FIFO#(E_channel) ff_xactor_E <- mkSizedFIFO(2);
`endif

	Reg#(MeshWIdx) rg_x_id <- mkRegU();
	Reg#(MeshHIdx) rg_y_id <- mkRegU();

interface core_side = interface Ifc_core_side_slave_link;
	interface slave_req_A = toGet(ff_xactor_A);
	interface slave_resp_B = toPut(ff_xactor_B);
	interface slave_req_C = toGet(ff_xactor_C);
	interface slave_resp_D = toPut(ff_xactor_D);
	interface slave_ack_E = toGet(ff_xactor_E);
	method Action coords(Len ids);
		rg_x_id <=ids.x_len; 
		rg_y_id <=ids.y_len; 
	endmethod
endinterface;

interface fabric_side = interface Ifc_fabric_side_slave_link;
	interface fabric_req_A = toPut(ff_xactor_A);
	interface fabric_resp_B = toGet(ff_xactor_B);
	interface fabric_req_C = toPut(ff_xactor_C);
	interface fabric_resp_D = toGet(ff_xactor_D);
	interface fabric_ack_E = toPut(ff_xactor_E);
	method Len coords;
		return Len {x_len : rg_x_id, y_len : rg_y_id};
	endmethod
endinterface;

endmodule

//---------------------------------------------- NIC Input -------------------------------------//
						//-----------------------MASTER CONNECTION----------------------------//
instance Connectable#(Ifc_fabric_side_master_link#(a,w,z), NetworkOuterInterface)
					provisos (Add#(a__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(2, TAdd#(a, TAdd#(TDiv#(w, 8),
    																																								w)))))), 128),																							
										Add#(z, TAdd#(w, b__), 114),
										Add#(TDiv#(w, 8), TAdd#(z, TAdd#(a, TAdd#(w, c__))), 120));																							
	module mkConnection#(Ifc_fabric_side_master_link#(a,w,z) node, NetworkOuterInterface mesh)(Empty)
					provisos (Add#(a__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(2, TAdd#(a, TAdd#(TDiv#(w, 8),
    																																								w)))))), 128),
										Add#(b__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(2, TAdd#(5, TAdd#(w, 1)))))), 128),	
										Add#(z, TAdd#(w, d__), 114),
									 Add#(TDiv#(w, 8), TAdd#(z, TAdd#(a, TAdd#(w, c__))), 120));																							
		
		rule rl_transfer_A;
			A_channel#(a,w,z) lv_A <- node.fabric_req_A.get();
			Bit#(DataSz) data = zeroExtend(pack(lv_A));
			LookAheadRouteInfo routeInfo = address_map(lv_A.a_address, A,
																									node.coords.x_len, node.coords.y_len,
																									lv_A.a_source, 0); 
																								 
			Flit flit = Flit { msgType : Control0,
												 vc : 0,
												 flitType : HeadTail, 
												 routeInfo : routeInfo, 
												 flitData : data};
			$display("flitdata %h", data);
			mesh.putFlit(flit);
		endrule

		//TODO should add support for channel B
		//rule rl_transfer_B;
		//	Flit flit <- mesh.getFlit();
		//	Bit#(SizeOf#(B_channel)) lv_B = unpack(truncate(flit.data);
		//	node.fabric_req_B.put(lv_B)
		//	
		//endrule

		//rule rl_transfer_C;
		//	C_channel lv_C <- node.fabric_req_C.put()
		//	Bit#(SizeOf#(C_channel)) data = pack(lv_C);
		//	LookAheadRouteInfo routeInfo = address_map(lv_C.a_address, C,
		//																							node.x_len, node.y_len,
		//																							lv_C.a_source, lv_C.a_sink); 
		//																						 
		//	Flit flit = Flit { msgType : Control,
		//										 vc : 0,
		//										 flitType : HeadTail, 
		//										 routeInfo : routeInfo, 
		//										 flitData : zeroExtend(data)};
		//	mesh.putFlit(flit);
		//endrule

		rule rl_transfer_D;
			Flit flit <- mesh.getFlit();
			D_channel#(w,z) lv_D = unpack(truncate(flit.flitData));
			node.fabric_resp_D.put(lv_D);
		endrule

		//rule rl_transfer_E;
		//	E_channel lv_E <- node.fabric_req_E.put()
		//	Bit#(SizeOf#(E_channel)) data = pack(lv_E);
		//	LookAheadRouteInfo routeInfo = address_map(0, E,
		//																							node.x_len, node.y_len,
		//																							0, lv_E.a_sink); 
		//	Flit flit = Flit { msgType : Control,
		//										 vc : 0,
		//										 flitType : HeadTail, 
		//										 routeInfo : routeInfo, 
		//										 flitData : zeroExtend(data)};
		//	mesh.putFlit(flit);
		//endrule
	endmodule
endinstance

						//-----------------------SLAVE CONNECTION----------------------------//
instance Connectable#(NetworkOuterInterface, Ifc_fabric_side_slave_link#(a,w,z)) 
					provisos (Add#(a__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(2, TAdd#(a, TAdd#(TDiv#(w, 8),
    																																								w)))))), 128),																							
										Add#(b__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(2, TAdd#(5, TAdd#(w, 1)))))), 128),	
										Add#(z, TAdd#(w, b__), 114),
										Add#(TDiv#(w, 8), TAdd#(z, TAdd#(a, TAdd#(w, c__))), 120));																							
	module mkConnection#(NetworkOuterInterface mesh, Ifc_fabric_side_slave_link#(a,w,z) node)(Empty)
					provisos (Add#(a__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(2, TAdd#(a, TAdd#(TDiv#(w, 8),
    																																								w)))))), 128),																							
										Add#(b__, TAdd#(3, TAdd#(3, TAdd#(z, TAdd#(2, TAdd#(5, TAdd#(w, 1)))))), 128),	
										Add#(z, TAdd#(w, d__), 114),
									 Add#(TDiv#(w, 8), TAdd#(z, TAdd#(a, TAdd#(w, a__))), 120));																							
																												
		
		rule rl_transfer_A;
			Flit flit <- mesh.getFlit();
			A_channel#(a,w,z) lv_A = unpack(truncate(flit.flitData));
			node.fabric_req_A.put(lv_A);
		endrule

		//rule rl_transfer_B;
		//	Flit flit <- mesh.getFlit();
		//	Bit#(SizeOf#(B_channel)) lv_B = unpack(truncate(flit.data);
		//	node.fabric_req_B.put(lv_B)
		//	
		//endrule

		//rule rl_transfer_C;
		//	C_channel lv_C <- node.fabric_req_C.put()
		//	Bit#(SizeOf#(C_channel)) data = pack(lv_C);
		//	LookAheadRouteInfo routeInfo = address_map(lv_C.a_address, C,
		//																							node.x_len, node.y_len,
		//																							lv_C.a_source, lv_C.a_sink); 
		//																						 
		//	Flit flit = Flit { msgType : Control,
		//										 vc : 0,
		//										 flitType : HeadTail, 
		//										 routeInfo : routeInfo, 
		//										 flitData : zeroExtend(data)};
		//	mesh.putFlit(flit);
		//endrule

		rule rl_transfer_D;
			D_channel#(w,z) lv_D <- node.fabric_resp_D.get();
			Bit#(DataSz) data = zeroExtend(pack(lv_D));
			Bit#(a) addr =0;
			LookAheadRouteInfo routeInfo = address_map(addr, D,
																									node.coords.x_len, node.coords.y_len,
																									lv_D.d_source, lv_D.d_sink); 
																								 
			Flit flit = Flit { msgType : Control0,
												 vc : 0,
												 flitType : HeadTail, 
												 routeInfo : routeInfo, 
												 flitData : data};
			mesh.putFlit(flit);
		endrule

		//rule rl_transfer_E;
		//	E_channel lv_E <- node.fabric_req_E.put()
		//	Bit#(SizeOf#(E_channel)) data = pack(lv_E);
		//	LookAheadRouteInfo routeInfo = address_map(0, E,
		//																							node.x_len, node.y_len,
		//																							0, lv_E.a_sink); 
		//	Flit flit = Flit { msgType = Control;
		//										 vc = 0;
		//										 flitType = HeadTail; 
		//										 routeInfo = routeInfo; 
		//										 flitData = zeroExtend(data);
		//	mesh.putFlit(flit);
		//endrule
	endmodule
endinstance

endpackage
